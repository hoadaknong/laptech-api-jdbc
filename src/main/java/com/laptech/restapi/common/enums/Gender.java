package com.laptech.restapi.common.enums;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}

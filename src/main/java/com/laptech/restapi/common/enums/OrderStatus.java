package com.laptech.restapi.common.enums;

public enum OrderStatus {
    PENDING,
    WAIT_CONFIRMED,
    PREPARED,
    SHIPPED,
    RECEIVED,
    CANCELED,
    FAILED,
    IGNORED
}

package com.laptech.restapi.common.enums;


public enum ImageType {
    ADVERTISE,
    DETAIL,
    EXTRA,
    FEEDBACK
}

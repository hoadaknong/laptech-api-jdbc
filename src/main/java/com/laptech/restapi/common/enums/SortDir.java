package com.laptech.restapi.common.enums;

public enum SortDir {
    ASC, DESC
}

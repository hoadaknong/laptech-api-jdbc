package com.laptech.restapi.common.enums;

public enum RoleType {
    ADMIN, MANAGER, EDITOR, USER
}

package com.laptech.restapi.common.enums;

public enum DiscountType {
    PRODUCT,
    PURCHASE
}

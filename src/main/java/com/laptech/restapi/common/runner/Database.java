package com.laptech.restapi.common.runner;

import com.laptech.restapi.common.enums.Gender;
import com.laptech.restapi.model.User;
import com.laptech.restapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


//@Component
    @RequiredArgsConstructor
public class Database implements CommandLineRunner {
    private final UserService userService;


    @Override
    public void run(String... args) {
        List<User> userList = new ArrayList<>();

        User user1 = new User(0L,
                "Phạm Dinh Quốc Hòa",
                Gender.MALE,
                LocalDate.parse("2001-06-25"),
                "+84388891635",
                null,
                "123456",
                true
        );
        userList.add(user1);

        userList.forEach(user -> {
            User newUser = userService.insert(user);
            System.out.println(newUser.toString());
        });
    }
}

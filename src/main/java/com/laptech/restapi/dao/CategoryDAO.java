package com.laptech.restapi.dao;

import com.laptech.restapi.dto.filter.CategoryFilter;
import com.laptech.restapi.model.Category;


public interface  CategoryDAO extends BaseDAO<Category, CategoryFilter, Long> {
}

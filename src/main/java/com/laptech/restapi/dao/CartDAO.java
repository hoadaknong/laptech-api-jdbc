package com.laptech.restapi.dao;

import com.laptech.restapi.dto.filter.BaseFilter;
import com.laptech.restapi.model.Cart;


public interface CartDAO extends BaseDAO<Cart, BaseFilter, String> {
    Cart findCartByUserId(long userId);
}

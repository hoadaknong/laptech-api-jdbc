package com.laptech.restapi.dao;

import com.laptech.restapi.dto.filter.FeedbackFilter;
import com.laptech.restapi.model.Feedback;

import java.util.Collection;


public interface FeedbackDAO extends BaseDAO<Feedback, FeedbackFilter, String> {
    Collection<Feedback> findFeedbackByProductId(String productId);

    Collection<Feedback> findFeedbackByUserId(long userId);
}

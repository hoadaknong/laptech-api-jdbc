package com.laptech.restapi.dao;

import com.laptech.restapi.dto.filter.LabelFilter;
import com.laptech.restapi.model.Label;

import java.util.Collection;


public interface LabelDAO extends BaseDAO<Label, LabelFilter, Long> {
    Collection<Label> findLabelByProductId(String productId);
}

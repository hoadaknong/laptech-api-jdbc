package com.laptech.restapi.dao;

import com.laptech.restapi.dto.filter.CommentFilter;
import com.laptech.restapi.model.Comment;

import java.util.Collection;


public interface CommentDAO extends BaseDAO<Comment, CommentFilter, String> {
    Collection<Comment> findCommentByProductId(String productId);

    /**
     * Comment will be gotten by user phone number
     */
    Collection<Comment> findCommentByUserPhone(String phone);
}

package com.laptech.restapi.dao;

import com.laptech.restapi.dto.filter.BrandFilter;
import com.laptech.restapi.model.Brand;


public interface BrandDAO extends BaseDAO<Brand, BrandFilter, Long> {
}

package com.laptech.restapi.dao;

import com.laptech.restapi.common.dto.PagingOptionDTO;
import com.laptech.restapi.common.dto.SortOptionDTO;

import java.util.Collection;


public interface BaseDAO<T, F extends SortOptionDTO, ID> {
    ID insert(T t); // create -> return id

    int update(T t);

    int delete(ID id, String updateBy);

    long count();

    long countWithFilter(F filter);

    boolean isExists(T t);

    Collection<T> findAll(PagingOptionDTO pagingOption);

    default Collection<T> findAll() {
        return this.findAll(new PagingOptionDTO(null, null, null, null));
    }

    Collection<T> findWithFilter(F filter);

    T findById(ID id);
}

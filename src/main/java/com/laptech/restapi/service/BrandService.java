package com.laptech.restapi.service;

import com.laptech.restapi.model.Brand;

public interface BrandService extends BaseService<Brand, Long> {
}

package com.laptech.restapi.service;

import com.laptech.restapi.model.Category;


public interface CategoryService extends BaseService<Category, Long> {
}

package com.laptech.restapi.service;

import com.laptech.restapi.model.Comment;

import java.util.Collection;



public interface CommentService extends BaseService<Comment, String> {
    Collection<Comment> getAllCommentsOfProduct(String productId);

    Collection<Comment> getAllCommentsOfUser(String phone);
}

package com.laptech.restapi.service;

import com.laptech.restapi.model.ImportProduct;

import java.util.Collection;


public interface ImportProductService extends BaseService<ImportProduct, String> {
    Collection<ImportProduct> findImportProductByProductId(String productId);
}

package com.laptech.restapi.service;

import com.laptech.restapi.dto.response.ProductUnitCardDTO;
import com.laptech.restapi.model.ProductUnit;

import java.math.BigDecimal;
import java.util.Collection;

public interface ProductUnitService extends BaseService<ProductUnit, String> {
    void updateProductUnitProperties(String unitId, int quantity, BigDecimal price, BigDecimal discountPrice, String updateBy);

    Collection<ProductUnit> getProductUnitByCartId(String cartId);

    Collection<ProductUnit> getProductUnitByInvoiceId(String invoiceId);

    Collection<ProductUnitCardDTO> getProductUnitCardCollection(Collection<ProductUnit> collection);
}

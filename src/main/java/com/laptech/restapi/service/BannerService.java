package com.laptech.restapi.service;

import com.laptech.restapi.model.Banner;


public interface BannerService extends BaseService<Banner, Long> {
}

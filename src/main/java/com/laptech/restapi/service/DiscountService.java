package com.laptech.restapi.service;

import com.laptech.restapi.model.Discount;

import java.util.Collection;


public interface DiscountService extends BaseService<Discount, Long> {
    Collection<Discount> getDiscountsOfProduct(String productId);
}

package com.laptech.restapi.service;

import com.laptech.restapi.model.Cart;


public interface CartService extends BaseService<Cart, Long> {
}

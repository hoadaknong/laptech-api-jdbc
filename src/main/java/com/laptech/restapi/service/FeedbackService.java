package com.laptech.restapi.service;

import com.laptech.restapi.model.Feedback;

import java.util.Set;

public interface FeedbackService extends BaseService<Feedback, String> {
    Set<?> getAllFeedbacksOfProduct(String productId);

    Set<Feedback> getAllFeedbacksOfUser(long userId);
}

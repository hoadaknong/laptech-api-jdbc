package com.laptech.restapi.service.payment.checkout;

import com.laptech.restapi.dto.request.CheckoutDTO;

public interface CheckoutService {
    void checkout(CheckoutDTO checkoutDTO);
}

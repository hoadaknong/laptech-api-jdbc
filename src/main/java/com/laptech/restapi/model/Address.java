package com.laptech.restapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address extends BaseModel {
    private String id;
    private long userId;
    private String country;
    private String line1;
    private String line2;
    private String line3;
    private String street;
    private boolean isDefault;

    @Override
    public String toString() {
        return "Address{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                ", country='" + country + '\'' +
                ", line1='" + line1 + '\'' +
                ", line2='" + line2 + '\'' +
                ", line3='" + line3 + '\'' +
                ", street='" + street + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", '" + super.toString() + '\'' +
                '}';
    }
}

package com.laptech.restapi.model;

import com.laptech.restapi.common.enums.ImageType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductImage extends BaseModel {
    private String id;
    private String productId;
    private String feedbackId;
    private String url;
    private ImageType type;

    @Override
    public String toString() {
        return "ProductImage{" +
                "id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", feedbackId='" + feedbackId + '\'' +
                ", url='" + url + '\'' +
                ", type=" + type +
                "} " + super.toString();
    }
}

package com.laptech.restapi.jwt.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtRequest {
    private String phone;
    private String password;
}
